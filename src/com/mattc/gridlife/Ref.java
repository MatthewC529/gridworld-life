package com.mattc.gridlife;

import java.awt.Dimension;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JFrame;

import com.mattc.gridlife.io.SaveManager;
import com.mattc.gridlife.world.GridDimensions;

public final class Ref {

	private Ref(){}
	
	public static final String TITLE = "Conway's Game Of GridWorld Life";
	public static final String AUTHOR = "Matthew Crocco";
	public static final String VERSION = "v0.0.1a";
	
	public static SaveManager recentSave;
	
	public static JFrame FRAME;
	public static final AtomicInteger GENERATION = new AtomicInteger(0);
	
	public static Dimension DEF_FRAME_DIM;
	public static Dimension FRAME_DIM;
	
	public static final GridDimensions DEF_GRID_DIM = new GridDimensions(40, 60);
	public static GridDimensions GRID_DIM = DEF_GRID_DIM;
	
	public static final String getFullTitle(){
		return TITLE + " " + VERSION;
	}
	
	public static final String getFullTitleWithAuthor(){
		return TITLE + " " + VERSION + " | By: " + AUTHOR; 
	}
	
}
