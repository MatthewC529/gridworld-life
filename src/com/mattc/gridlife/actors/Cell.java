package com.mattc.gridlife.actors;

import info.gridworld.actor.Actor;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import com.mattc.gridlife.GUI;
import com.mattc.gridlife.GridWorldLife;
import com.mattc.gridlife.util.Colour;

public class Cell extends Actor{

	public enum CellState{
		ON(Colour.GREEN), OFF(Colour.TRANSPARENT), DISABLED(Colour.BLACK);
		
		public final Colour color;
		
		private CellState(Colour color){
			this.color = color;
		}
		
	}
	
	private CellState state;
	private CellManager manager = GridWorldLife.instance.CELLS;
	
	public Cell(int r, int c, CellState startState, Grid<Actor> grid){
		super();
		this.state = startState;
		this.setColor(state.color);
		this.putSelfInGrid(grid, new Location(r, c));
	}
	
	public void toggle(){
		if(state == CellState.DISABLED) return;
		
		if(state == CellState.ON)
			setState(CellState.OFF);
		else
			setState(CellState.ON);
	}
	
	public void disable(){
		this.state = CellState.DISABLED;
	}
	
	public void setState(CellState state){
		this.state = state;
		
		act();
		GUI.gridRepaint();
	}
	
	public CellState getState(){
		return state;
	}
	
	public boolean isActive(){
		return state == CellState.ON;
	}
	
	@Override
	public void act(){
		if(state == null) return;
		this.setColor(state.color);
	}
	
}
