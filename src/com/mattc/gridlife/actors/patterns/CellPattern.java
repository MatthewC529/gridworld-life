package com.mattc.gridlife.actors.patterns;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;

public interface CellPattern {
	
	public enum Type{
		STABLE,
		MOVING,
		GUN,
		TEMPORARY,
		PRODUCE;
	}

	public CellState[][] getPattern();
	public Point getCentralIndices();
	public String getPatternName();
	public Type getPatternType();
	
}
