package com.mattc.gridlife.actors.patterns;

import info.gridworld.actor.Actor;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import com.mattc.gridlife.GridWorldLife;
import com.mattc.gridlife.actors.Cell;
import com.mattc.gridlife.actors.Cell.CellState;

public class PatternGenerator {
	
	public static void generate(CellPattern pattern, Location gLoc){
		int x = pattern.getCentralIndices().x;
		int y = pattern.getCentralIndices().y;
		
		Grid<Actor> gr = GridWorldLife.instance.world.getGrid();
		CellState[][] states = pattern.getPattern();
		
		if(x >= states[0].length || x < 0 || y >= states.length || y < 0)
			throw new BadPatternException("Bad Central Indices Value! x = " + x + ", y = " + y);
	
		clearArea(states, x, y, gLoc);
		
		for(int r = 0; r < states.length; r++){
			for(int c = 0; c < states[0].length; c++){
				int row = gLoc.getRow() + (r - y);
				int col = gLoc.getCol() + (c - x);
				
				Location tmp = new Location(row, col);
				if(!gr.isValid(tmp)) continue;
				
				Actor a = gr.get(tmp);
				if(a instanceof Cell){
					((Cell) a).setState(states[r][c]);
				}
			}
		}
	}
	
	private static void clearArea(CellState[][] states, int x, int y, Location gLoc){
		
		Grid<Actor> gr = GridWorldLife.instance.world.getGrid();
		
		for(int r = 0; r < states.length; r++){
			for(int c = 0; c < states[0].length; c++){
				int row = gLoc.getRow() + (r - y);
				int col = gLoc.getCol() + (c - x);
				
				Location tmp = new Location(row, col);
				if(!gr.isValid(tmp)) continue;
				
				Actor a = gr.get(tmp);
				if(a instanceof Cell){
					((Cell) a).setState(CellState.OFF);
				}
			}
		}
	}
	
}
