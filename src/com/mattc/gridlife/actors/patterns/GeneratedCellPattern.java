package com.mattc.gridlife.actors.patterns;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;

public class GeneratedCellPattern implements CellPattern{

	public final CellState[][] pattern;
	public final CellPattern.Type type;
	public final String name;
	public final Point point;
	
	public GeneratedCellPattern(CellState[][] pattern, CellPattern.Type type, String name, Point center) {
		this.pattern = pattern;
		this.type = type;
		this.name = name;
		this.point = center;
	}
	
	@Override
	public CellState[][] getPattern() {
		return pattern;
	}

	@Override
	public Point getCentralIndices() {
		return point;
	}

	@Override
	public String getPatternName() {
		return name;
	}

	@Override
	public Type getPatternType() {
		return type;
	}

}
