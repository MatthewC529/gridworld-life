package com.mattc.gridlife.actors.patterns;

public class BadPatternException extends RuntimeException{

	public BadPatternException(){
		super("Bad Pattern Generation!");
	}
	
	public BadPatternException(String msg){
		super("Bad Pattern Generation! " + msg);
	}
	
}
