package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;

public class BeaconPattern implements CellPattern{

	CellState[][] pattern = {
			{ON, ON, OFF, OFF},
			{ON, ON, OFF, OFF},
			{OFF, OFF, ON, ON},
			{OFF, OFF, ON, ON}
	};
	
	@Override
	public CellState[][] getPattern() {
		return pattern;
	}

	@Override
	public Point getCentralIndices() {
		return new Point(0, 0);
	}

	@Override
	public String getPatternName() {
		return "Beacon";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.STABLE;
	}

}
