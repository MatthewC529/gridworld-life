package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;
import com.mattc.gridlife.util.ArrayUtils;

public class TumblerPattern implements CellPattern {

	public enum Orientation{
		NORTH(TumblerPattern.n_pattern, "North-Facing", new Point(11, 8)),
		WEST(TumblerPattern.w_pattern, "West-Facing", new Point(8, 11)),
		EAST(TumblerPattern.e_pattern, "East-Facing", new Point(1, 11)),
		SOUTH(TumblerPattern.s_pattern, "South-Facing", new Point(11, 1));
		
		public final CellState[][] pattern;
		public final String pname;
		public final Point p;
		
		private Orientation(CellState[][] pattern, String name, Point p){
			this.pattern = pattern;
			this.pname = name;
			this.p = p;
		}
	}
	
	static CellState[][] n_pattern = {
		{OFF,  ON,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
		{ ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON},
		{ ON, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF,  ON,  ON},
		{ ON, OFF, OFF,  ON, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF},
		{OFF, OFF,  ON, OFF,  ON, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF},
		{OFF, OFF, OFF,  ON, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON, OFF,  ON, OFF, OFF},
		{OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON, OFF, OFF, OFF, OFF},
		{OFF, OFF, OFF, OFF,  ON,  ON,  ON, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
		{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
		{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF}
	};
	
	static CellState[][] w_pattern = ArrayUtils.rotateAndPreserve(n_pattern);
	static CellState[][] s_pattern = ArrayUtils.flipOverYAndPreserve(ArrayUtils.flipOverXAndPreserve(n_pattern));
	static CellState[][] e_pattern = ArrayUtils.flipOverXAndPreserve(ArrayUtils.flipOverYAndPreserve(w_pattern));
	
	private final TumblerPattern.Orientation orient;
	
	public TumblerPattern(TumblerPattern.Orientation orient) {
		this.orient = orient;
	}
	
	@Override
	public CellState[][] getPattern() {
		return orient.pattern;
	}

	@Override
	public Point getCentralIndices() {
		return orient.p;
	}

	@Override
	public String getPatternName() {
		return orient.pname + " Tumbler";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.STABLE;
	}

}
