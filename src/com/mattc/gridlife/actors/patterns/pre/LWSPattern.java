package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;

public class LWSPattern implements CellPattern {
	
	public static final byte RIGHT = 0x01;
	public static final byte LEFT = 0x00;
	
	CellState[][] left_pattern = {
			{OFF, ON, OFF, OFF, ON},
			{ON, OFF, OFF, OFF, OFF},
			{ON, OFF, OFF, OFF, ON},
			{ON, ON, ON, ON, OFF}
	};
	
	CellState[][] right_pattern={
			{ON, OFF, OFF, ON, OFF},
			{OFF, OFF, OFF, OFF, ON},
			{ON, OFF, OFF, OFF, ON},
			{OFF, ON, ON, ON, ON}
	};
	
	private final byte choice;
	public LWSPattern(byte selection){
		choice = selection;
	}
	
	@Override
	public CellState[][] getPattern() {
		if(choice == RIGHT)
			return right_pattern;
		else
			return left_pattern;
	}

	@Override
	public Point getCentralIndices() {
		return new Point(0,0);
	}

	@Override
	public String getPatternName() {
		return "LW Spaceship (" + (choice == RIGHT ? "Right" : "Left") + ")";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.MOVING;
	}

}
