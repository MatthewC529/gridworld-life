package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;

public class MathematicianPattern implements CellPattern {

	CellState[][] pattern = {
			{OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF,  ON, OFF,  ON, OFF, OFF, OFF},
			{OFF, OFF, OFF,  ON, OFF,  ON, OFF, OFF, OFF},
			{OFF, OFF,  ON,  ON, OFF,  ON,  ON, OFF, OFF},
			{ ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON},
			{ ON,  ON,  ON, OFF, OFF, OFF,  ON,  ON,  ON},
			{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
			{ ON,  ON,  ON,  ON,  ON,  ON,  ON,  ON,  ON},
			{ ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON},
			{OFF, OFF, OFF,  ON,  ON,  ON,  ON, OFF, OFF},
			{OFF, OFF, OFF,  ON, OFF, OFF,  ON,  ON, OFF}
	};
	
	@Override
	public CellState[][] getPattern() {
		return pattern;
	}

	@Override
	public Point getCentralIndices() {
		return new Point(4, 5);
	}

	@Override
	public String getPatternName() {
		return "Mathematician";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.STABLE;
	}

}
