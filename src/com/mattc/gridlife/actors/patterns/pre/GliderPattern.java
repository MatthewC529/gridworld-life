package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;

public class GliderPattern implements CellPattern{

	public enum GliderDirection{
		SE(GliderPattern.se_pattern), 
		SW(GliderPattern.sw_pattern), 
		NE(GliderPattern.ne_pattern), 
		NW(GliderPattern.nw_pattern);
		
		public final CellState[][] pattern;
		
		private GliderDirection(CellState[][] pattern){
			this.pattern = pattern;
		}
	}
	
	static CellState[][] se_pattern ={
			{OFF, ON, OFF},
			{OFF, OFF, ON},
			{ON, ON, ON}
	};
	
	static CellState[][] sw_pattern = {
		{OFF, ON, OFF},
		{ON, OFF, OFF},
		{ON, ON, ON}
	};
	
	static CellState[][] ne_pattern = {
			{ON, ON, ON},
			{OFF, OFF, ON},
			{OFF, ON, OFF}
	};
	
	static CellState[][] nw_pattern = {
			{ON, ON, ON},
			{ON, OFF, OFF},
			{OFF, ON, OFF}
	};
	
	private GliderDirection dir;
	
	public GliderPattern(GliderDirection dir) {
		this.dir = dir;
	}
	
	@Override
	public CellState[][] getPattern() {
		return dir.pattern;
	}

	@Override
	public Point getCentralIndices() {
		return new Point(1, 1);
	}

	@Override
	public String getPatternName() {
		return "Glider (" + dir.name() + ")";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.MOVING;
	}

}
