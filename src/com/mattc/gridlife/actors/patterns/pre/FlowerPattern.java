package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;

public class FlowerPattern implements CellPattern{
	
	CellState[][] pattern = {
			{OFF,  ON,  ON,  ON, OFF, OFF,  ON,  ON, OFF, OFF, OFF},
			{OFF,  ON,  ON, OFF,  ON, OFF,  ON, OFF,  ON,  ON,  ON},
			{OFF,  ON,  ON,  ON, OFF, OFF,  ON,  ON,  ON,  ON,  ON},
			{ ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON},
			{ ON,  ON,  ON,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF},
			{OFF, OFF, OFF, OFF,  ON,  ON,  ON, OFF, OFF, OFF, OFF},
			{OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON,  ON,  ON,  ON},
			{ ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON},
			{ ON,  ON,  ON,  ON,  ON, OFF, OFF,  ON,  ON,  ON, OFF},
			{ ON,  ON,  ON, OFF,  ON, OFF,  ON, OFF,  ON,  ON, OFF},
			{OFF, OFF, OFF,  ON,  ON, OFF, OFF,  ON,  ON,  ON, OFF}
	};
	
	@Override
	public CellState[][] getPattern() {
		return pattern;
	}

	@Override
	public Point getCentralIndices() {
		return new Point(5, 5);
	}

	@Override
	public String getPatternName() {
		return "Flower of Eden";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.TEMPORARY;
	}

}
