package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;
import com.mattc.gridlife.util.ArrayUtils;

public class BrainPattern implements CellPattern{

	public enum BrainOrientation{
		N(BrainPattern.n_pattern, new Point(9, 5)),
		S(BrainPattern.s_pattern, new Point(9, 5)),
		E(BrainPattern.e_pattern, new Point(5, 9)),
		W(BrainPattern.w_pattern, new Point(5, 9));
		
		public final CellState[][] pattern;
		public final Point center;
		
		private BrainOrientation(CellState[][] pattern, Point p){
			this.pattern = pattern;
			this.center = p;
		}
	}
	
	static CellState[][] n_pattern = {
			{OFF,  ON,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON,  ON, OFF},
			{ ON, OFF,  ON, OFF,  ON,  ON, OFF, OFF, OFF, OFF, OFF,  ON,  ON, OFF,  ON, OFF,  ON},
			{ ON, OFF,  ON, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF,  ON, OFF,  ON},
			{OFF,  ON, OFF,  ON,  ON, OFF,  ON,  ON, OFF,  ON,  ON, OFF,  ON,  ON, OFF,  ON, OFF},
			{OFF, OFF, OFF, OFF, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF, OFF, OFF},
			{OFF, OFF,  ON,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF , ON, OFF,  ON,  ON, OFF, OFF},
			{OFF, OFF,  ON,  ON,  ON, OFF, OFF,  ON, OFF,  ON, OFF, OFF,  ON,  ON,  ON, OFF, OFF},
			{OFF, OFF,  ON,  ON, OFF, OFF,  ON, OFF, OFF, OFF,  ON, OFF, OFF,  ON,  ON, OFF, OFF},
			{OFF,  ON, OFF, OFF ,OFF ,OFF,  ON,  ON, OFF,  ON,  ON, OFF, OFF, OFF, OFF,  ON, OFF},
			{OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF}
	};
	
	static CellState[][] w_pattern = ArrayUtils.rotateAndPreserve(n_pattern);
	static CellState[][] s_pattern = ArrayUtils.flipOverXAndPreserve(n_pattern);
	static CellState[][] e_pattern = ArrayUtils.flipOverYAndPreserve(w_pattern);
	
	private final BrainOrientation orient;
	
	public BrainPattern(BrainOrientation orient) {
		this.orient = orient;
	}
	
	@Override
	public CellState[][] getPattern() {
		return orient.pattern;
	}

	@Override
	public Point getCentralIndices() {
		return orient.center;
	}

	@Override
	public String getPatternName() {
		return orient.name() + " Brain";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.MOVING;
	}

}
