package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;
import com.mattc.gridlife.util.ArrayUtils;

public class ChemistPattern implements CellPattern{
	
	public enum Orientation{
		NORMAL(ChemistPattern.normal_pattern, "Normal"),
		FLIPPED(ChemistPattern.reversed_pattern, "Reversed");
		
		public final CellState[][] pattern;
		public final String pname;
		
		private Orientation(CellState[][] pattern, String name){
			this.pattern = pattern;
			this.pname = name;
		}
	}
	
	static CellState[][] normal_pattern = {
			{OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON,  ON, OFF, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF, OFF, OFF,  ON,  ON,  ON, OFF, OFF,  ON, OFF, OFF,  ON,  ON},
			{OFF, OFF, OFF, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON},
			{OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF,  ON, OFF,  ON, OFF,  ON, OFF, OFF},
			{OFF,  ON,  ON, OFF,  ON, OFF, OFF, OFF, OFF, OFF,  ON, OFF,  ON,  ON, OFF},
			{OFF, OFF,  ON, OFF,  ON, OFF,  ON, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF},
			{ ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF,  ON, OFF, OFF, OFF, OFF},
			{ ON,  ON, OFF, OFF,  ON, OFF, OFF,  ON,  ON,  ON, OFF, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF, OFF, OFF,  ON,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF ,OFF ,OFF, OFF, OFF, OFF}
	};
	
	static CellState[][] reversed_pattern = ArrayUtils.flipOverYAndPreserve(normal_pattern);
	
	private final ChemistPattern.Orientation orient;
	
	public ChemistPattern(ChemistPattern.Orientation orient){
		this.orient = orient;
	}
	
	@Override
	public CellState[][] getPattern() {
		return orient.pattern;
	}

	@Override
	public Point getCentralIndices() {
		return new Point(7, 6);
	}

	@Override
	public String getPatternName() {
		return orient.pname + " Chemist";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.STABLE;
	}
	
	
	
}
