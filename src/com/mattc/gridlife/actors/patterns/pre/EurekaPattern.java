package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;
import com.mattc.gridlife.util.ArrayUtils;

public class EurekaPattern implements CellPattern{

	public enum Orientation{
		VERTICAL(EurekaPattern.rotated, "Vertical", new Point(5, 9)),
		HORIZONTAL(EurekaPattern.pattern, "Horizontal", new Point(9, 5));
		
		public final CellState[][] pattern;
		public final String pname;
		public final Point center;
		
		private Orientation(CellState[][] pattern, String name, Point p){
			this.pattern = pattern;
			this.pname = name;
			this.center = p;
		}
	}
	
	static CellState[][] pattern = {
			{OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF},
			{ ON, OFF,  ON, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF,  ON},
			{OFF,  ON, OFF, OFF, OFF,  ON,  ON, OFF,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF},
			{OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
			{OFF, OFF, OFF, OFF, OFF, OFF ,OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
			{OFF,  ON, OFF, OFF, OFF,  ON,  ON, OFF,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF},
			{ ON, OFF,  ON, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF,  ON},
			{OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF}
	};
	
	static CellState[][] rotated = ArrayUtils.rotateAndPreserve(pattern);
	
	private final EurekaPattern.Orientation orient;
	
	public EurekaPattern(EurekaPattern.Orientation orient){
		this.orient = orient;
	}
	
	@Override
	public CellState[][] getPattern() {
		return orient.pattern;
	}

	@Override
	public Point getCentralIndices() {
		return orient.center;
	}

	@Override
	public String getPatternName() {
		return orient.pname + " Eureka!";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.STABLE;
	}

	
	
}
