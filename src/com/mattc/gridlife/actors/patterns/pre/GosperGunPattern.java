package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;
import com.mattc.gridlife.util.ArrayUtils;

public class GosperGunPattern implements CellPattern{

	public enum Aiming{
		SW(GosperGunPattern.sw_pattern),
		SE(GosperGunPattern.se_pattern),
		NW(GosperGunPattern.nw_pattern),
		NE(GosperGunPattern.ne_pattern);
		
		public final CellState[][] pattern;
		
		private Aiming(CellState[][] pattern){
			this.pattern = pattern;
		}
	}
	
	static CellState[][] se_pattern = {
	{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
	{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
	{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON},
	{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON},
	{ ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
	{ ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF,  ON, OFF,  ON,  ON, OFF, OFF, OFF, OFF,  ON, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
	{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
	{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON, OFF, OFF, OFF,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF},
	{OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF,  ON,  ON, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF, OFF}
	};
	static CellState[][] ne_pattern = ArrayUtils.flipOverXAndPreserve(se_pattern);
	static CellState[][] sw_pattern = ArrayUtils.flipOverYAndPreserve(se_pattern);
	static CellState[][] nw_pattern = ArrayUtils.flipOverXAndPreserve(sw_pattern);
	
	
	private final Aiming aim;
	public GosperGunPattern(Aiming aim) {
		this.aim = aim;
	}
	
	@Override
	public CellState[][] getPattern() {
		return aim.pattern;
	}

	@Override
	public Point getCentralIndices() {
		return new Point(19, 5);
	}

	@Override
	public String getPatternName() {
		return "Gosper Gun (Shooting: " + aim.name() + ")";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.GUN;
	}

}
