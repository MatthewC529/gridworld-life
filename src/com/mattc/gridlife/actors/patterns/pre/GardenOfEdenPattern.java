package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;
import com.mattc.gridlife.actors.patterns.CellPattern.Type;

public class GardenOfEdenPattern implements CellPattern{

	CellState[][] pattern = {
			{OFF,  ON, OFF,  ON,  ON,  ON, OFF,  ON, OFF, OFF},
			{OFF, OFF,  ON, OFF,  ON, OFF,  ON, OFF, OFF,  ON},
			{ ON, OFF,  ON,  ON,  ON, OFF, OFF,  ON,  ON, OFF},
			{OFF,  ON, OFF,  ON,  ON,  ON,  ON,  ON, OFF,  ON},
			{ ON, OFF, OFF,  ON, OFF, OFF,  ON,  ON,  ON,  ON},
			{ ON,  ON,  ON,  ON, OFF, OFF,  ON, OFF, OFF,  ON},
			{OFF,  ON,  ON, OFF, OFF,  ON,  ON,  ON, OFF,  ON},
			{ ON, OFF, OFF,  ON, OFF,  ON, OFF,  ON, OFF, OFF},
			{OFF, OFF,  ON, OFF,  ON,  ON,  ON, OFF,  ON, OFF}
	};
	
	@Override
	public CellState[][] getPattern() {
		return pattern;
	}

	@Override
	public Point getCentralIndices() {
		return new Point(4, 4);
	}

	@Override
	public String getPatternName() {
		return "Garden of Eden";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.TEMPORARY;
	}

}
