package com.mattc.gridlife.actors.patterns.pre;

import static com.mattc.gridlife.actors.Cell.CellState.OFF;
import static com.mattc.gridlife.actors.Cell.CellState.ON;

import java.awt.Point;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.CellPattern;

public class FigureEightPattern implements CellPattern{

	public enum Orientation{
		NORMAL(FigureEightPattern.pattern, "N-Slope"),
		FLIPPED(FigureEightPattern.flipped, "P-Slope");
		
		public final CellState[][] pattern;
		public final String pname;
		
		private Orientation(CellState[][] pattern, String name){
			this.pattern = pattern;
			this.pname = name;
		}
	}
	
	static CellState[][] pattern = {
		{ ON,  ON, OFF, OFF, OFF, OFF},
		{ ON,  ON, OFF,  ON, OFF, OFF},
		{OFF, OFF, OFF, OFF,  ON, OFF},
		{OFF,  ON, OFF, OFF, OFF, OFF},
		{OFF, OFF,  ON, OFF,  ON,  ON},
		{OFF, OFF, OFF, OFF,  ON,  ON}
	};
	
	static CellState[][] flipped = {
		{OFF, OFF, OFF, OFF,  ON,  ON},
		{OFF, OFF,  ON, OFF,  ON,  ON},
		{OFF,  ON, OFF, OFF, OFF, OFF},
		{OFF, OFF, OFF, OFF,  ON, OFF},
		{ ON,  ON, OFF,  ON, OFF, OFF},
		{ ON,  ON, OFF, OFF, OFF, OFF}
	};
	
	private final FigureEightPattern.Orientation orient;
	
	public FigureEightPattern(FigureEightPattern.Orientation orient) {
		this.orient = orient;
	}
	
	@Override
	public CellState[][] getPattern() {
		return orient.pattern;
	}

	@Override
	public Point getCentralIndices() {
		return new Point(3, 3);
	}

	@Override
	public String getPatternName() {
		return orient.pname + " Figure Eight";
	}

	@Override
	public Type getPatternType() {
		return CellPattern.Type.STABLE;
	}

	
	
}
