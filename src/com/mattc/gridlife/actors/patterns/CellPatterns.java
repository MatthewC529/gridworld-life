package com.mattc.gridlife.actors.patterns;

import java.awt.Point;
import java.lang.reflect.Field;
import java.util.ArrayList;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.patterns.pre.BeaconPattern;
import com.mattc.gridlife.actors.patterns.pre.BeehivePattern;
import com.mattc.gridlife.actors.patterns.pre.BrainPattern;
import com.mattc.gridlife.actors.patterns.pre.BrainPattern.BrainOrientation;
import com.mattc.gridlife.actors.patterns.pre.ChemistPattern;
import com.mattc.gridlife.actors.patterns.pre.EurekaPattern;
import com.mattc.gridlife.actors.patterns.pre.FigureEightPattern;
import com.mattc.gridlife.actors.patterns.pre.FlowerPattern;
import com.mattc.gridlife.actors.patterns.pre.GardenOfEdenPattern;
import com.mattc.gridlife.actors.patterns.pre.GliderPattern;
import com.mattc.gridlife.actors.patterns.pre.GliderPattern.GliderDirection;
import com.mattc.gridlife.actors.patterns.pre.GosperGunPattern;
import com.mattc.gridlife.actors.patterns.pre.GosperGunPattern.Aiming;
import com.mattc.gridlife.actors.patterns.pre.LWSPattern;
import com.mattc.gridlife.actors.patterns.pre.MathematicianPattern;
import com.mattc.gridlife.actors.patterns.pre.PulsarPattern;
import com.mattc.gridlife.actors.patterns.pre.TumblerPattern;
import com.mattc.gridlife.actors.patterns.pre.VolcanoPattern;
import com.mattc.gridlife.util.Console;

public class CellPatterns {

	private static final ArrayList<CellPattern> generated = new ArrayList<CellPattern>();
	
	public static final CellPattern BEACON = new BeaconPattern();
	public static final CellPattern BEEHIVE = new BeehivePattern();
	public static final CellPattern FIGURE_EIGHT = new FigureEightPattern(FigureEightPattern.Orientation.NORMAL);
	public static final CellPattern FLIPPED_FIGURE_EIGHT = new FigureEightPattern(FigureEightPattern.Orientation.FLIPPED);
	public static final CellPattern PULSAR = new PulsarPattern();
	public static final CellPattern MATHEMATICIAN = new MathematicianPattern();
	public static final CellPattern NORMAL_CHEMIST = new ChemistPattern(ChemistPattern.Orientation.NORMAL);
	public static final CellPattern FLIPPED_CHEMIST = new ChemistPattern(ChemistPattern.Orientation.FLIPPED);
	public static final CellPattern HORIZONTAL_EUREKA = new EurekaPattern(EurekaPattern.Orientation.HORIZONTAL);
	public static final CellPattern VERTICAL_EUREKA = new EurekaPattern(EurekaPattern.Orientation.VERTICAL);
	public static final CellPattern VOLCANO = new VolcanoPattern();
	public static final CellPattern N_TUMBLER = new TumblerPattern(TumblerPattern.Orientation.NORTH);
	public static final CellPattern W_TUMBLER = new TumblerPattern(TumblerPattern.Orientation.WEST);
	public static final CellPattern S_TUMBLER = new TumblerPattern(TumblerPattern.Orientation.SOUTH);
	public static final CellPattern E_TUMBLER = new TumblerPattern(TumblerPattern.Orientation.EAST);
	public static final CellPattern S_WEST_GLIDER = new GliderPattern(GliderDirection.SW);
	public static final CellPattern S_EAST_GLIDER = new GliderPattern(GliderDirection.SE);
	public static final CellPattern N_WEST_GLIDER = new GliderPattern(GliderDirection.NW);
	public static final CellPattern N_EAST_GLIDER = new GliderPattern(GliderDirection.NE);
	public static final CellPattern N_BRAIN = new BrainPattern(BrainOrientation.N);
	public static final CellPattern S_BRAIN = new BrainPattern(BrainOrientation.S);
	public static final CellPattern E_BRAIN = new BrainPattern(BrainOrientation.E);
	public static final CellPattern W_BRAIN = new BrainPattern(BrainOrientation.W);
	public static final CellPattern LW_SPACESHIP_WEST = new LWSPattern(LWSPattern.LEFT);
	public static final CellPattern LW_SPACESHIP_EAST = new LWSPattern(LWSPattern.RIGHT);
	public static final CellPattern FLOWER_OF_EDEN = new FlowerPattern();
	public static final CellPattern GARDEN_OF_EDEN = new GardenOfEdenPattern();
	public static final CellPattern SW_GOSPER_GUN = new GosperGunPattern(Aiming.SW);
	public static final CellPattern NW_GOSPER_GUN = new GosperGunPattern(Aiming.NW);
	public static final CellPattern SE_GOSPER_GUN = new GosperGunPattern(Aiming.SE);
	public static final CellPattern NE_GOSPER_GUN = new GosperGunPattern(Aiming.NE);
	
	public static void registerPattern(CellPattern pattern){
		if(!generated.contains(pattern))
			generated.add(pattern);
	}
	
	/**
	 * Translates a Two-Dimensional Array of Characters into a Two-Dimensional Array of CellStates and then to a CellPattern.
	 * @param patternName - Name To Give to Pattern
	 * @param patternType - Type Of Pattern
	 * @param arr - Array to Convert
	 * @param on - On Character
	 * @param off - Off Character
	 * @param center - Central Point to Generate From
	 * @return CellPattern
	 */
	public static CellPattern fromCharArray(String patternName, CellPattern.Type patternType, char[][] arr, char on, char off, Point center){
		CellState[][] states = new CellState[arr.length][arr[0].length];
		
		for(int r = 0; r < states.length; r++){
			for(int c = 0; c < states[0].length; c++){
				if(arr[r][c] != on && arr[r][c] != off)
					throw new BadPatternException("Bad Character! " + String.format("%s != %s or %s", arr[r][c], on, off));
				
				if(arr[r][c] == on)
					states[r][c] = CellState.ON;
				else if(arr[r][c] == off)
					states[r][c] = CellState.OFF;
				else{
					Console.error("INVALID CHARACTER: Bad Pattern! " + String.format("%s is not %s or %s", arr[r][c], on, off));
				}
			}
		}
		
		return new GeneratedCellPattern(states, patternType, patternName, center);
	}
	
	/**
	 * Converts a Two-Dimensional Character Array to a Two-Dimensional CellState Array to Generate a CellPattern.
	 * @param name - Name of Pattern
	 * @param type - Type of Pattern
	 * @param pattern - Array to Convert
	 * @param on - On Character
	 * @param off - Off Character
	 * @param centerColumn - Column of Central Point
	 * @param centerRow - Row of Central Point
	 * @return
	 */
	public static CellPattern fromCharArray(String name, CellPattern.Type type, char[][] pattern, char on, char off, int centerColumn, int centerRow){
		return CellPatterns.fromCharArray(name, type, pattern, on, off, new Point(centerColumn, centerRow));
	}
	
	/**
	 * Gets All Registered and Hardcoded Patterns.
	 * @return
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	public static CellPattern[] getAllPatterns() throws IllegalArgumentException, IllegalAccessException {
		
		ArrayList<CellPattern> patterns = new ArrayList<CellPattern>();
		patterns.addAll(generated);
		
		Field[] fields = CellPatterns.class.getDeclaredFields();
		
		for(Field f: fields)
			if(f.getType() == CellPattern.class)
				patterns.add((CellPattern) f.get(null));
		
		return patterns.toArray(new CellPattern[patterns.size()]);
	}
	
}
