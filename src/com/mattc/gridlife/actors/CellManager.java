package com.mattc.gridlife.actors;

import info.gridworld.actor.Actor;
import info.gridworld.grid.Location;
import info.gridworld.world.World;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.listeners.ActListener;
import com.mattc.gridlife.util.Console;

public class CellManager implements ActListener{

	public final Set<Cell> active;
	public final World<Actor> world;
	
	public CellManager(int seeds, World<Actor> world){
		this.world = world;
		this.active = new HashSet<Cell>(world.getGrid().getNumCols() * world.getGrid().getNumRows());
		generate(seeds);
	}
	
	private void generate(int seeds){
		Random prng = new Random(System.currentTimeMillis());
		int rows = world.getGrid().getNumRows();
		int cols = world.getGrid().getNumCols();
		
		Console.info("Generating Inactive Cells...");;
		for(int r = 0; r < rows; r++){
			for(int c = 0; c < cols; c++){
				new Cell(r, c, CellState.OFF, world.getGrid());
			}
		}
		
		if(seeds > (rows * cols))
			seeds = (rows * cols);
		
		Console.info("Seeding Active Cells...");
		while(seeds > 0){
			int r = rows-1;
			int c = cols-1;
			
			r *= prng.nextFloat();
			c *= prng.nextFloat();
			
			Cell cell = ((Cell) world.getGrid().get(new Location(r, c)));
			if(!cell.isActive()){
				cell.setState(CellState.ON);
				seeds--;
			}
		}
	}

	@Override
	public void onAct() {
		int rows = world.getGrid().getNumRows();
		int cols = world.getGrid().getNumCols();
		ArrayList<Cell> marked = new ArrayList<Cell>();
		
		for(int r = 0; r < rows; r++){
			for(int c = 0; c < cols; c++){
				Actor tmp = world.getGrid().get(new Location(r, c));
				if(tmp instanceof Cell){
					if(determineMark((Cell) tmp)){
						marked.add((Cell)tmp);
					}
				}
			}
		}
		
		for(int r = 0; r < rows; r++){
			for(int c = 0; c < cols; c++){
				Actor tmp = world.getGrid().get(new Location(r, c));
				if(tmp instanceof Cell){
					if(determineBirth((Cell) tmp)){
						marked.add((Cell) tmp);
					}
				}
			}
		}	
		
		for(Cell c: marked)
			c.toggle();
		
	}
	
	private boolean determineMark(Cell c){
		if(c.getState() == CellState.OFF) return false;
		ArrayList<Location> list = world.getGrid().getOccupiedAdjacentLocations(c.getLocation());
		ArrayList<Location> tmp = new ArrayList<Location>(list);
		
		for(Location l: tmp){
			Cell a = (Cell)world.getGrid().get(l);
			if(a.getState() == CellState.OFF)
				list.remove(l);
		}
		
		if(list.size() < 2 || list.size() > 3)
			return true;
		else 
			return false;
	}
	
	private boolean determineBirth(Cell c){
		if(c.getState() == CellState.ON) return false;
		ArrayList<Location> list = world.getGrid().getOccupiedAdjacentLocations(c.getLocation());
		ArrayList<Location> tmp = new ArrayList<Location>(list);
		
		for(Location l: tmp){
			Cell a = (Cell) world.getGrid().get(l);
			if(a.getState() == CellState.OFF)
				list.remove(l);
		}
		
		if(list.size() == 3)
			return true;
		else
			return false;
	}
	
	private void determine(Cell c){
		ArrayList<Location> list = world.getGrid().getOccupiedAdjacentLocations(c.getLocation());
		ArrayList<Location> tmp = new ArrayList<>(list);
		
		for(Location l: tmp){
			Actor a = world.getGrid().get(l);
			if(!(a instanceof Cell)){
				list.remove(l);
			}else{
				Cell cTmp = (Cell) a;
				if(cTmp.getState() == CellState.OFF)
					list.remove(l);
			}
		}
		
		if(c.getState() == CellState.ON && (list.size() < 2 || list.size() > 3)){
			c.setState(CellState.OFF);
		}
		
		if(c.getState() == CellState.OFF){
			if(list.size() == 3)
				c.setState(CellState.ON);
		}
	}
	
}
