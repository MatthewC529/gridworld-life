package com.mattc.gridlife.util;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * A Toolkit of Various Array Utilities that are used throughout this Program <br />
 * <br />
 * Including Generic Array Creation and Array Editing Methods.
 * 
 * @author Matthew
 *
 */
public final class ArrayUtils {

	private ArrayUtils(){}
	
	public static <T> T[][] rotateAndPreserve(T[][] array){
		T[][] tmp = createArray(array[0].getClass().getComponentType(), array[0].length, array.length);
		
		for(int i = 0 ; i < array.length; i++){
			T[] arr = array[i];
			for(int p = 0 ; p < arr.length; p++){
				tmp[p][i] = arr[p];
			}
		}
		
		return tmp;
	}
	
	/**
	 * Flips an Array around point as if around a vertical axis. Preserves Order of Original Array and returns the flipped copy.
	 * @param arr
	 * @return Flipped Copy of Original Array
	 */
	public static <T> T[] flipOverYAndPreserve(T[] arr){
		
		T[] tmp = ArrayUtils.createArray(arr.getClass().getComponentType(), arr.length);
		int z = arr.length-1;
		
		for(int a = 0; a < arr.length; a++){
			tmp[z] = arr[a];
			z--;
		}
		
		return tmp;
	}
	
	/**
	 * Flips a 2D Array as if around a vertical axis. Preserves Order of Original 2D Array and returns flipped copy.
	 * @param arr
	 * @return Flipped Copy of 2D Array
	 */
	public static <T> T[][] flipOverYAndPreserve(T[][] arr){
		T[][] tmp = ArrayUtils.createArray(arr[0].getClass().getComponentType(), arr.length, arr[0].length);
		
		for(int i = 0 ; i < arr.length; i++){
			tmp[i] = flipOverYAndPreserve(arr[i]);
		}
		
		return tmp;
	}
	
	/**
	 * Flips the 2D Array as if around a Vertical Axis. Does not preserve original array.
	 * @param arr
	 */
	public static <T> void flipOverY(T[][] arr){
		for(int i = 0; i < arr.length; i++){
			flipOverY(arr[i]);
		}
	}
	
	/**
	 * Flips Array as if around a Vertical Axis. Does NOT preserve original array
	 * @param arr
	 */
	public static <T> void flipOverY(T[] arr){
		int z = arr.length - 1;
		
		for(int a = 0; a < z; a++){
			T tmp = arr[a];
			arr[a] = arr[z];
			arr[z] = tmp;
			z--;
		}
	}
	
	/**
	 * Flips 2D Array as if around a Horizontal Axis. Preserves Original 2D Array and returns a Flipped Copy.
	 * @param arr
	 * @return Flipped Copy of 2D Array
	 */
	public static <T> T[][] flipOverXAndPreserve(T[][] arr){
		T[][] temp = ArrayUtils.createArray(arr[0].getClass().getComponentType(), arr.length, arr[0].length);
		int z = arr.length-1;
		
		for(int a = 0; a < arr.length; a++){
			temp[a] = arr[z];
			z--;
		}
		
		return temp;
	}
	
	/**
	 * Flips 2D Array as if around a Horizontal Axis. Does NOT Preserve Original 2D Array.
	 * @param arr
	 */
	public static <T> void flipOverX(T[][] arr){
		int z = arr.length - 1;
		
		for(int a = 0; a < z; a++){
			T[] tmp = arr[a];
			arr[a] = arr[z];
			arr[z] = tmp;
		}
	}
	
	/**
	 * Dereferences (Reference Type -> Value Type) a Character Array into a char Array
	 * @param arr
	 * @return
	 */
	public static char[] dereferenceArray(Character[] arr){
		char[] tmp = new char[arr.length];
		
		for(int i = 0 ; i < tmp.length; i++){
			tmp[i] = arr[i];
		}
		
		return tmp;
	}
	
	/**
	 * Removes an Object from an Array
	 * @param arr
	 * @param obj
	 * @return
	 */
	public static <T> T[] removeFromArray(T[] arr, T obj){
		for(int i = 0; i < arr.length; i++){
			if(arr[i] == obj)
				return removeFromArray(arr, i);
		}
		
		return arr;
	}
	
	/**
	 * Removes an Object at a Specific Index from an Array
	 * @param arr
	 * @param index
	 * @return
	 */
	public static <T> T[] removeFromArray(T[] arr, int index){
		
		T[] tmp = ArrayUtils.createArray(arr.getClass().getComponentType(), arr.length-1);
		int tIndex = 0;
		for(int i = 0; i < arr.length; i++){
			if(i == index) continue;
			
			tmp[tIndex++] = arr[i];
		}
		
		arr = tmp;
		return arr;
	}
	
	public static String toString(Object[][] arr){
		String s = "{";
		for(int i = 0; i < arr.length; i++){
			if(i == arr.length-1)
				s += Arrays.toString(arr[i]);
			else
				s += Arrays.toString(arr[i]) + ", ";
		}
		s += "}";
		return s;
	}
	
	/**
	 * Create New One-Dimensional Array Using Reflection Tools
	 * @param clazz
	 * @param size
	 * @return
	 */
	public static <T> T[] createArray(Class<?> clazz, int size){
		return (T[]) Array.newInstance(clazz, size);
	}
	
	/**
	 * Create New Two-DImensional Array Using Reflection Tools
	 * @param clazz
	 * @param sizeOne
	 * @param sizeTwo
	 * @return
	 */
	public static <T> T[][] createArray(Class<?> clazz, int sizeOne, int sizeTwo){
		return (T[][]) Array.newInstance(clazz, sizeOne, sizeTwo);
	}
	
}
