package com.mattc.gridlife.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * A Tool Class <Br /> 
 * <br />
 * Mostly describes various Utilities ranging from String Manipulation to Grid Manipulation <br />
 * yet mostly describes Reflection Utilities to permit clean-ish code.
 * @author Matthew Crocco
 *
 */
public final class Utility {

	private Utility(){} 
	
	/**
	 * Grab Field Value from a Given Instance (Or Static Variable if Null) using Reflection API
	 * @param clazz
	 * @param instance
	 * @param fieldName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getFieldValue(Class<?> clazz, Object instance, String fieldName){
		
		Field[] fields = clazz.getDeclaredFields();
		
		//Scan Fields for the one we are looking for.
		for(Field f: fields){
			if(f.getName().equalsIgnoreCase(fieldName)){
				try{	
					boolean accessDenied = false;
					
					if(!f.isAccessible()){
						f.setAccessible(true);
						accessDenied = true;
					}
					
					T val = (T) f.get(instance);
					
					if(accessDenied)
						f.setAccessible(false);
					
					return val;
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null; //Failure to Grab Value!
	}
	
	/**
	 * Instantiates a Default Instance of the given Class or Null
	 * @param clazz
	 * @return
	 */
	public static <T> T getDefaultInstanceOf(Class<? extends T> clazz){
		try {
			return (T)clazz.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Gets an Instance of the Given Class using a Constructor with the specified Arguments
	 * @param clazz
	 * @param params
	 * @return
	 */
	public static <T> T getInstanceOf(Class<? extends T> clazz, Object... params){
		
		if(params.length == 0 || params[0] == null){
			return getDefaultInstanceOf(clazz);
		}
		
		Constructor<?>[] constructors = clazz.getConstructors();
		HashSet<Class<?>> paramSet = new HashSet<Class<?>>();
		
		for(Object o: params){
			paramSet.add(o.getClass());
		}
		
		for(Constructor<?> c : constructors){
			
			if(c.getParameterTypes().length == paramSet.size()){
				
				boolean good = true;
				for(Class<?> p1 : paramSet)
					for(Class<?> p2 : c.getParameterTypes())
						if(!p1.getName().equals(p2.getName())){
							good = false;
							break;
						}
				
				if(good){
					boolean accessDenied = false;
					if(!c.isAccessible()){
						c.setAccessible(true);
						accessDenied = true;
					}
					
					try{
						T val = (T) c.newInstance(params);
						if(accessDenied)
							c.setAccessible(false);
						
						return val;
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			
		}
		
		return null;
	}
	
	/**
	 * Set Field Value to a given value  in the given Instance (or Static Variable if Null)
	 * @param clazz
	 * @param instance
	 * @param name
	 * @param value
	 */
	public static void setFieldValue(Class<?> clazz, Object instance, String name, Object value){
		
		Field[] fields = clazz.getDeclaredFields();
		
		for(Field f: fields){
			boolean cracked = false;
			if(f.getName().equalsIgnoreCase(name)){
				if(!f.isAccessible()){
					cracked = true;
					f.setAccessible(true);
				}
				
				try{
					f.set(instance, value);
				}catch(Exception e){
					Console.exception(e);
				}
				
				if(cracked){
					f.setAccessible(false);
				}
			}
		}
		
	}
	
	public static <T> T executeMethod(Class<?> clazz, Object instance, String name){
		Method[] meths = clazz.getDeclaredMethods();
		
		for(Method m: meths){
			boolean cracked = false;
			if(m.getName().contains(name)){
				if(!m.isAccessible()){
					m.setAccessible(true);
					cracked = true;
				}
				try {
					T val = (T) m.invoke(instance, null);
					if(cracked)
						m.setAccessible(false);
					
					return val;
				} catch (Exception e){
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
	/**
	 * Attempt to Execute Method in the Given Class and Instance (Or Null) of the given name and with the given arguments
	 * @param clazz
	 * @param instance
	 * @param name
	 * @param args
	 * @return
	 */
	public static <T> T executeMethod(Class<?> clazz, Object instance, String name, Object... args){
		
		ArrayList<Class<?>> argTypes = new ArrayList<Class<?>>();
		ArrayList<Method> possible = new ArrayList<Method>();
		Method[] methods = clazz.getDeclaredMethods();
		
		for(Object o: args){
			argTypes.add(o.getClass());
		}
		
		for(Method m: methods){
			if(m.getName().equalsIgnoreCase(name))
				possible.add(m);
		}
		
		Class<?>[] types = argTypes.toArray(new Class<?>[argTypes.size()]);
		for(Method m: possible){
			boolean broken = false;
			Class<?>[] paramsTypes = m.getParameterTypes();
			if(paramsTypes.length != types.length) continue;
			
			for(int i = 0; i < paramsTypes.length; i++){
				if(!paramsTypes[i].getSimpleName().equals(types[i].getSimpleName().toLowerCase())){
					broken = true;
					break;
				}
			}
			
			if(broken) continue;
			
			try {
				return (T) m.invoke(instance, args);
			} catch(Exception e){
				Console.exception(e);
			}
		}
		
		return null;
	}
	
	/**
	 * Returns True if All Alphabetic Characters are Uppercase
	 * @param str
	 * @return
	 */
	public static boolean capsCheck(String str){
		
		char[] arr = str.toCharArray();
		
		for(char c: arr){
			if(!Character.isAlphabetic(c)) continue;
			if(Character.isLowerCase(c)) return false;
		}
		
		return true;
		
	}
	
	/**
	 * Currently Just Capitalizes the First letter in name. camelCase to TitleCase.
	 * @param str
	 * @return
	 */
	public static String toDisplayName(String str){
		if(capsCheck(str)) return str;
		
		StringBuilder sb = new StringBuilder();
		char[] arr = str.toCharArray();
		
		for(int i = 0; i < arr.length; i++){
			if(i == 0){
				sb.append(Character.toUpperCase(arr[i]));
				continue;
			}
			
			sb.append(arr[i]);
		}
		
		return sb.toString();
	}
	
}
