package com.mattc.gridlife.util;

/**
 * Describes an Auto-Looped Thread that can be Terminated at any time. <br />
 * <br />
 * Follows what is recommended in the Java Standard Documentation and uses a variable to determine when to terminate. <br />
 * Call {@link TerminalThread#terminate()} when you want this Thread to exit!
 * @author Matthew Crocco
 */
public abstract class TerminalThread extends Thread{

	boolean running = false;
	
	@Override
	public void start(){
		running = true;
		super.start();
	}
	
	@Override
	public final void run(){
		while(running){
			commit();
		}
	}
	
	public boolean isRunning(){
		return running;
	}
	
	public void terminate(){
		running = false;
	}
	
	public abstract void commit(); //Replaces Thread.run() as the "Functioning" method
	
}
