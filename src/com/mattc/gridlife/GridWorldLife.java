package com.mattc.gridlife;

import info.gridworld.actor.Actor;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;

import java.awt.MouseInfo;
import java.awt.Point;
import java.util.ArrayList;

import com.mattc.gridlife.actors.Cell;
import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.actors.CellManager;
import com.mattc.gridlife.util.Console;
import com.mattc.gridlife.util.TerminalThread;
import com.mattc.gridlife.world.LifeWorld;

//TODO Completely Re-Work Control Panel
public class GridWorldLife {

	public static GridWorldLife instance;
	public static volatile boolean brush = false;
	
	public final CellManager CELLS;
	public final LifeWorld world;
	public final MouseThread thread;
	
	protected GridWorldLife(){
		
		GridWorldLife.instance = this;
		this.world = new LifeWorld();
		
		Console.init();
		GUI.init(world);
		
		CELLS = new CellManager(0, world);
		this.world.addActListener(CELLS);
		thread = new MouseThread();
		thread.start();
		
		Console.info("Initialization Complete!");
		
	}
	
	public void clearBoard(){
		Grid<Actor> grid = world.getGrid();
		
		ArrayList<Location> list = grid.getOccupiedLocations();
		
		for(Location l: list){
			Actor a = grid.get(l);
			if(a instanceof Cell)
				((Cell) a).setState(CellState.OFF);
		}
	}
	
	public void setWorldState(CellState[][] states){
		Grid<Actor> grid = world.getGrid();
		
		for(int r = 0; r < states.length; r++){
			for(int c = 0; c < states[0].length; c++){
				Console.info(String.format("LOADING: Row %s Column %s State=%s", r, c, states[r][c]));
				Location l = new Location(r, c);
				Actor a = grid.get(new Location(r, c));
				if(!grid.isValid(l))
					throw new RuntimeException("BAD LOCATION!");
				if(a instanceof Cell)
					((Cell) a).setState(states[r][c]);
			}
		}
	}
	
	public CellState[][] generateWorldState(){
		Grid<Actor> grid = world.getGrid();
		CellState[][] states = new CellState[grid.getNumRows()][grid.getNumCols()];
		int rows = states.length;
		int cols = states[0].length;
		
		for(int r = 0; r < rows; r++){
			for(int c = 0; c < cols; c++){
				Actor a = grid.get(new Location(r, c));
				if(a instanceof Cell){
					states[r][c] = ((Cell) a).getState();
				}else
					states[r][c] = CellState.OFF;
			}
		}
		
		return states;
	}
	
	public static void main(String[] args){
		new GridWorldLife();
	}
	
}

class MouseThread extends TerminalThread{
	
	public void commit(){
		Point p = MouseInfo.getPointerInfo().getLocation();
		p.x -= 290;
		p.y -= 143;
		if(GridWorldLife.brush){
			Location l = GUI.panel.locationForPoint(p);
			
			Grid<Actor> grid = GridWorldLife.instance.world.getGrid();
			Actor tmp = grid.get(l);
			if(tmp instanceof Cell)
				((Cell) tmp).setState(CellState.ON);
		}
		GUI.panel.repaint();
	}
	
}
