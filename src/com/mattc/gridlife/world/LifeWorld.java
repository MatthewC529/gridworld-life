package com.mattc.gridlife.world;

import info.gridworld.actor.ActorWorld;

import java.util.ArrayList;

import com.mattc.gridlife.Ref;
import com.mattc.gridlife.listeners.ActListener;

public class LifeWorld extends ActorWorld{

	public ArrayList<ActListener> listeners;
	
	public LifeWorld(){
		super(new LifeGrid(Ref.GRID_DIM.rows, Ref.GRID_DIM.columns));
		listeners = new ArrayList<ActListener>();
		show();
	}
	
	public void step(){
		super.step();
		for(ActListener listener: listeners){
			listener.onAct();
		}
	}
	
	public void addActListener(ActListener listener){
		listeners.add(listener);
	}
	
	public void removeActListener(ActListener listener){
		listeners.remove(listener);
	}
	
	public void clearActListeners(){
		listeners.clear();
	}
	
}
