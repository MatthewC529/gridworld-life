package com.mattc.gridlife.world;

import info.gridworld.actor.Actor;
import info.gridworld.grid.BoundedGrid;
import info.gridworld.grid.Location;

public class LifeGrid extends BoundedGrid<Actor>{

	public LifeGrid(int r, int c){
		super(r, c);
	}
	
	@Override
	public Actor get(Location l){
		if(!isValid(l)) return null;
		else return super.get(l);
	}
	
}
