package com.mattc.gridlife.world;

public class GridDimensions {

	public int rows, columns;
	
	public GridDimensions(int rows, int columns){
		this.rows = rows;
		this.columns = columns;
	}
	
	public GridDimensions copy(){
		return new GridDimensions(rows, columns);
	}
	
}
