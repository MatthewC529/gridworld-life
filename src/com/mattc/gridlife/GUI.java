package com.mattc.gridlife;

import info.gridworld.actor.Actor;
import info.gridworld.grid.Location;
import info.gridworld.gui.DisplayMap;
import info.gridworld.gui.GUIController;
import info.gridworld.gui.GridPanel;
import info.gridworld.gui.WorldFrame;
import info.gridworld.world.World;

import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ResourceBundle;

import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;

import com.mattc.gridlife.actors.patterns.CellPattern;
import com.mattc.gridlife.actors.patterns.CellPatterns;
import com.mattc.gridlife.io.SaveManager;
import com.mattc.gridlife.listeners.PatternCreationListener;
import com.mattc.gridlife.listeners.PatternListener;
import com.mattc.gridlife.listeners.SeedListener;
import com.mattc.gridlife.util.Console;
import com.mattc.gridlife.util.Utility;

public final class GUI {

	public static WorldFrame frame;
	public static GridPanel panel;
	public static GUIController control;
	public static JPopupMenu current;
	
	private GUI(){}
	
	public static final void init(World<Actor> world){
		Ref.FRAME = Utility.getFieldValue(World.class, world, "frame");
		GUI.frame = (WorldFrame) Ref.FRAME;
		GUI.panel = Utility.getFieldValue(WorldFrame.class, frame, "display");
		GUI.removeAllMouseListeners(GUI.panel);
		GUI.panel.getParent().setLayout(new FlowLayout());
		GUI.panel.addMouseListener(new SeedListener());
		GUI.panel.addMouseListener(new PatternListener());
		Utility.executeMethod(GridPanel.class, panel, "setToolTipsEnabled", false);
		GUI.control = new CustomController(frame);
	
		if(Ref.DEF_FRAME_DIM == null){
			firstResize();
			Ref.DEF_FRAME_DIM = frame.getSize();
			Ref.FRAME_DIM = Ref.DEF_FRAME_DIM;
		}else
			resize();
		
		center();
		
		frame.setTitle(Ref.getFullTitleWithAuthor());
		
		Utility.executeMethod(GridPanel.class, panel, "setToolTipsEnabled", false);
		initMenus();
		invokeLaters();
	}
	
	private static final void invokeLaters(){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				GUI.frame.setResizable(false);
			}
		});
	}
	
	private static final void initMenus(){
		Utility.<JMenu>getFieldValue(WorldFrame.class, frame, "newGridMenu").setEnabled(false);
		
		JMenuBar menuBar = frame.getJMenuBar();
		
		JMenuItem refresh = new JMenuItem("Refresh/Clear");
		JMenuItem aboutLife = new JMenuItem("About The Game Of Life...");
		JMenuItem save = new JMenuItem("Save World State");
		JMenuItem overwrite = new JMenuItem("Update Saved World State");
		JMenuItem load = new JMenuItem("Load World State");
		
		int menuMask = frame.getToolkit().getMenuShortcutKeyMask();
		
		KeyStroke refreshKey = KeyStroke.getKeyStroke(KeyEvent.VK_R, menuMask);
		KeyStroke exitKey = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		KeyStroke saveKey = KeyStroke.getKeyStroke(KeyEvent.VK_S, menuMask);
		KeyStroke loadKey = KeyStroke.getKeyStroke(KeyEvent.VK_L, menuMask);
		
		refresh.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GridWorldLife.instance.clearBoard();
			}
		});
		
		save.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					JFileChooser fc = new JFileChooser("./saves");
					int rval = fc.showSaveDialog(GUI.frame);
					SaveManager manager;
					
					if(rval == fc.APPROVE_OPTION){
						if(fc.getSelectedFile() == null || fc.getSelectedFile().getName().trim().startsWith(".") || fc.getSelectedFile().getName().trim().isEmpty()){
							manager = SaveManager.generateSaveFile();
						}else{
							String filename = fc.getSelectedFile().getAbsolutePath() + ".sav";
				
							File f = new File(filename);
							f.createNewFile();
							manager = new SaveManager(f);
						}
					}else if(rval == fc.CANCEL_OPTION){
						return;
					}else
						manager = SaveManager.generateSaveFile();

					manager.putSaveData(GridWorldLife.instance.generateWorldState());
					Ref.recentSave = manager;
				} catch (IOException e1) {
					Console.exception(e1);
				}
			}
		});
		
		overwrite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int returnVal = JOptionPane.showConfirmDialog(GUI.frame, "Use Most Recent Save?", "Overwrite Save", JOptionPane.YES_NO_CANCEL_OPTION);
				SaveManager saveManager;
				
				if(returnVal == JOptionPane.YES_OPTION && Ref.recentSave != null){
					saveManager = Ref.recentSave;
				}else if(returnVal == JOptionPane.YES_OPTION && Ref.recentSave == null){
					JOptionPane.showMessageDialog(GUI.frame, "Failure to Open Recent Save! It Does not Exist!... Opening FileChooser.", "No Recent Save!", JOptionPane.ERROR_MESSAGE);
					JFileChooser fc = new JFileChooser("./saves");
					fc.addChoosableFileFilter(SaveManager.getSaveFileFilter());
					int val = fc.showSaveDialog(GUI.frame);
					
					if(val == JFileChooser.APPROVE_OPTION){
						saveManager = new SaveManager(fc.getSelectedFile());
					}else return;
				}else if(returnVal == JOptionPane.NO_OPTION){
					JFileChooser fc = new JFileChooser("./saves");
					fc.addChoosableFileFilter(SaveManager.getSaveFileFilter());
					int val = fc.showSaveDialog(GUI.frame);
					
					if(val == JFileChooser.APPROVE_OPTION){
						saveManager = new SaveManager(fc.getSelectedFile());
					}else return;
				}else return;
				
				saveManager.putSaveData(GridWorldLife.instance.generateWorldState());
				Ref.recentSave = saveManager;
			}
		});
		
		load.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser("./saves");
				fc.addChoosableFileFilter(SaveManager.getSaveFileFilter());
				int returnVal = fc.showOpenDialog(GUI.frame);
				
				if(returnVal == JFileChooser.APPROVE_OPTION){
					File f = fc.getSelectedFile();
					SaveManager manager = new SaveManager(f);
					GridWorldLife.instance.setWorldState(manager.grabSaveData());
				}
			}
		});
		
		aboutLife.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				GUI.showAboutLife();
			}
		});
		
		refresh.setAccelerator(refreshKey);

		menuBar.getMenu(0).add(refresh, 1);
		menuBar.getMenu(2).add(aboutLife, 1);
		menuBar.getMenu(0).getItem(2).setText("Exit");
		menuBar.getMenu(0).getItem(2).setAccelerator(exitKey);
		menuBar.getMenu(0).add(save, 2);
		menuBar.getMenu(0).getItem(2).setAccelerator(saveKey);
		menuBar.getMenu(0).add(overwrite, 3);
		menuBar.getMenu(0).add(load, 4);
		menuBar.getMenu(0).getItem(4).setAccelerator(loadKey);
		menuBar.getMenu(0).add(new JSeparator(), 2);
		menuBar.getMenu(0).add(new JSeparator(), 6);
	}
	
	private static JPopupMenu generatePatternsPopup(Location l){
		JPopupMenu patterns = new JPopupMenu("Life Patterns:");
		try{
			CellPattern[] patternArray = CellPatterns.getAllPatterns();
			
			for(CellPattern p: patternArray){
				String text = String.format("%s - %s", p.getPatternName(), p.getPatternType().name());
				JMenuItem item = new JMenuItem(text);
				item.addActionListener(new PatternCreationListener(p, l, patterns));
				patterns.add(item);
				patterns.addSeparator();
			}
			
		}catch(Exception e){
			Console.exception(e);
		}
		
		return patterns;
	}
	
	private static void firstResize(){
		Utility.setFieldValue(GridPanel.class, panel, "cellSize", 12);
		int CELL_SIZE = Utility.getFieldValue(GridPanel.class, GUI.panel, "cellSize");
		int minWidth = (Ref.DEF_GRID_DIM.columns+1) * CELL_SIZE + 200;
		int minHeight = (Ref.DEF_GRID_DIM.rows+1) * CELL_SIZE + 200;
		
		GUI.panel.setSize((Ref.DEF_GRID_DIM.columns + 8) * CELL_SIZE, Ref.DEF_GRID_DIM.rows * CELL_SIZE);
		GUI.frame.setSize(minWidth, minHeight);
	}
	
	private static void center(){
		int sw = Toolkit.getDefaultToolkit().getScreenSize().width;
		int sh = Toolkit.getDefaultToolkit().getScreenSize().height;
		int fw = frame.getWidth()/2;
		int fh = frame.getHeight()/2;
		
		frame.setLocation((sw/2) - fw, (sh/2) - fh);
	}
	
	private static void resize(){
		int cellSize = Utility.getFieldValue(GridPanel.class, GUI.panel, "cellSize");
		int width = Ref.DEF_FRAME_DIM.width;
		int height = Ref.DEF_FRAME_DIM.height;
		
		int wOff = Ref.GRID_DIM.columns - Ref.DEF_GRID_DIM.columns;
		int hOff = Ref.GRID_DIM.rows - Ref.DEF_GRID_DIM.rows;
		
		wOff *= cellSize;
		hOff *= cellSize;
		
		width += wOff;
		height += hOff;
		
		GUI.frame.setSize(width, height);
	}
	
	public static final void invokeLater(Runnable invocation){
		EventQueue.invokeLater(invocation);
	}
	
	public static final void gridRepaint(){
		invokeLater(new Runnable() {
			public void run() {
				GUI.panel.repaint();
			}
		});
	}
	
	public static final void stopRunning(){
		GUI.control.stop();
	}
	
	public static final void removeAllMouseListeners(JComponent component){
		MouseListener[] listeners  = component.getMouseListeners();
		
		for(MouseListener l: listeners){
			component.removeMouseListener(l);
		}
		
	}
	
	public static void showAboutLife(){
		
		if(Desktop.isDesktopSupported()){
			try {
				Desktop.getDesktop().browse(new URI("http://conwaysgameoflife.appspot.com/"));
			} catch (Exception e){
				Console.exception(e);
			}
			return;
		}
		
		try{
			JDialog dialog = new JDialog(GUI.frame, "About Conway's Game of Life", false);
			JEditorPane wiki = new JEditorPane("http://conwaysgameoflife.appspot.com/");
			wiki.setEditable(false);
			JScrollPane scroll = new JScrollPane(wiki);
			
			dialog.add(scroll);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setLocation(10, 10);
			dialog.setSize(1200, 700);
			dialog.setVisible(true);
			
		}catch(IOException e){
			Console.fatal("Failure to grab Wiki Page!");
			Console.exception(e);
		}
	}

	public static void showPatternsPopup(Point point) {
		if(current != null){
			current.setVisible(false);
			current = null;
		}
		
		Location l = panel.locationForPoint(point);
		point = panel.pointForLocation(l);
		JPopupMenu menu = generatePatternsPopup(l);
		current = menu;
		
		menu.setLocation(point.x, 0);
		menu.setVisible(true);
	}
}

class CustomController extends GUIController<Actor> {
	
	WorldFrame frame;
	
	public CustomController(WorldFrame frame){
		super(frame, Utility.<GridPanel>getFieldValue(WorldFrame.class, frame, "display"), Utility.<DisplayMap>getFieldValue(WorldFrame.class, frame, "displayMap"), Utility.<ResourceBundle>getFieldValue(WorldFrame.class, frame, "resources"));
		this.frame = frame;
		
		replaceFrameController();
	}
	
	private void replaceFrameController(){
		Utility.setFieldValue(frame.getClass(), frame, "control", this);
	}
	
	@Override
	public void editLocation(){
		
	}
	
	@Override
	public String toString(){
		return "Custom Controller (No Popup)";
	}
	
}