package com.mattc.gridlife.listeners;

import info.gridworld.grid.Location;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPopupMenu;

import com.mattc.gridlife.actors.patterns.CellPattern;
import com.mattc.gridlife.actors.patterns.PatternGenerator;

//TODO
public class PatternCreationListener implements ActionListener {

	private final CellPattern pattern;
	private final Location location;
	private final JPopupMenu parent;
	
	public PatternCreationListener(final CellPattern p, final Location l, final JPopupMenu parent){
		this.pattern = p;
		this.location = l;
		this.parent = parent;
	}
	
	public void actionPerformed(ActionEvent e){
		parent.setVisible(false);
		PatternGenerator.generate(pattern, location);
	}
	
}
