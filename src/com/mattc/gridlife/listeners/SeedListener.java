package com.mattc.gridlife.listeners;

import info.gridworld.actor.Actor;
import info.gridworld.grid.Grid;
import info.gridworld.grid.Location;
import info.gridworld.gui.GridPanel;
import info.gridworld.gui.WorldFrame;
import info.gridworld.world.World;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.mattc.gridlife.GUI;
import com.mattc.gridlife.GridWorldLife;
import com.mattc.gridlife.Ref;
import com.mattc.gridlife.actors.Cell;
import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.util.Utility;

public class SeedListener extends MouseAdapter{

	GridPanel panel;
	World<Actor> world;
	private boolean brush = false;
	
	public SeedListener(){
		panel = Utility.getFieldValue(WorldFrame.class, Ref.FRAME, "display");
		world = GridWorldLife.instance.world;
	}
	
	@Override
	public void mouseClicked(MouseEvent evt){
		
		if(GUI.current != null){
			GUI.current.setVisible(false);
			GUI.current = null;
		}
	
		if(evt.getButton() == MouseEvent.BUTTON2){
			GridWorldLife.brush = !GridWorldLife.brush;
		}
		
		if(evt.getButton() == MouseEvent.BUTTON1){
			Grid<Actor> grid = world.getGrid();
			Location point = panel.locationForPoint(evt.getPoint());
			
			Actor tmp = grid.get(point);
			
			if(tmp instanceof Cell)
				((Cell) tmp).toggle();
		}
	}
	
	public void mouseMoved(MouseEvent evt){
		if(evt.getButton() == MouseEvent.BUTTON1){
			Grid<Actor> grid = world.getGrid();
			Location point = panel.locationForPoint(evt.getPoint());
			
			Actor tmp = grid.get(point);
			
			if(tmp instanceof Cell)
				((Cell) tmp).setState(CellState.ON);
		}
	}
}
