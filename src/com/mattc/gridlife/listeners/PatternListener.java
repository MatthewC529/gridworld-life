package com.mattc.gridlife.listeners;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.mattc.gridlife.GUI;

public class PatternListener extends MouseAdapter{
	
	public void mouseClicked(MouseEvent e){
		if(e.getButton() == MouseEvent.BUTTON3){
			GUI.showPatternsPopup(e.getPoint());
		}
	}
	
}
