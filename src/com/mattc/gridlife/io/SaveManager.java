package com.mattc.gridlife.io;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import javax.swing.filechooser.FileFilter;

import com.mattc.gridlife.GridWorldLife;
import com.mattc.gridlife.actors.Cell.CellState;
import com.mattc.gridlife.util.Console;

public final class SaveManager {

	public final File save;
	private static final FileFilter saveFileFilter = new FileFilter() {
		
		@Override
		public String getDescription() {
			return "SAV Files";
		}
		
		@Override
		public boolean accept(File f) {
			return f.getName().endsWith(".sav");
		}
	};
	
	public SaveManager(File savefile){
		this.save = savefile;
	}
	
	public static FileFilter getSaveFileFilter(){
		return saveFileFilter;
	}
	
	public static SaveManager generateSaveFile(String name) throws IOException{
		SimpleDateFormat sdf= new SimpleDateFormat("MM-dd-yy#HH-mm-ss");
		File dir = new File(new File("."), "saves");
		dir.mkdirs();
		String filename = String.format("gridlife#%s%s.sav", sdf.format(new Date()), (name == null || name.trim().isEmpty() ? "" : "--" + name));
		File save = new File(dir, filename);
		save.createNewFile();
		
		return new SaveManager(save);
	}
	
	public static SaveManager generateSaveFile() throws IOException{
		return generateSaveFile(null);
	}
	
	public CellState[][] grabSaveData(){
		Scanner scan = null;
		try{
			scan = new Scanner(save);
			CellState[][] states = new CellState[GridWorldLife.instance.world.getGrid().getNumRows()][GridWorldLife.instance.world.getGrid().getNumCols()];
			int row = -1;
			int column = 0;
			
			while(scan.hasNextLine() && row < states.length){
				String line = scan.nextLine();
				if(line.startsWith("#") || line.trim().isEmpty()) continue;
				row++;
				column = 0;
				String[] bits = line.split(" ");
				
				for(String s: bits){
					if(s.equals("1"))
						states[row][column] = CellState.ON;
					else if(s.equals("0"))
						states[row][column] = CellState.OFF;
					else continue;
					column++;
				}
			}
			
			return states;
		}catch(IOException e){
			Console.exception(e);
		}finally{
			if(scan != null)
				scan.close();
		}
		
		return null;
	}
	
	public void putSaveData(CellState[][] cdata){
		FileOutputStream fos = null;
		ByteArrayInputStream bis = null;
		try{
			byte[] buffer = new byte[2048];
			String[] data = statesToBits(cdata);
			fos = new FileOutputStream(save);
			for(String s: data){
				bis = new ByteArrayInputStream(s.getBytes());
				for(int c = bis.read(buffer); c > 0; c = bis.read(buffer))
					fos.write(buffer, 0, c);
				
				bis.close();
			}
		}catch(IOException e){
			Console.exception(e);
		}finally{
			try{
				if(fos != null){
					fos.close();
				}
				if(bis != null){
					bis.close();
				}
			}catch(IOException e){
				
			}
		}
	}
	
	private String[] statesToBits(CellState[][] data){
		String[] bits = new String[data.length];
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < data.length; i++){
			sb.delete(0, sb.length());
			CellState[] arr = data[i];
			for(CellState state: arr){
				if(state == CellState.ON)
					sb.append("1 ");
				else
					sb.append("0 ");
			}
			sb.append("\r\n");
			bits[i] = sb.toString();
		}
		
		return bits;
		
	}
	
}
